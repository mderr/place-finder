package com.example.placefinder

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.content.Intent
import android.widget.Button
import androidx.appcompat.app.AlertDialog

class InfoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info)

        val builder = AlertDialog.Builder(this)
        builder.setTitle("Información")
        builder.setMessage("Nuestro personal se acercará para ayudarte en breve")
        builder.setPositiveButton("Ok") { _, _ -> }

        val button: Button = findViewById(R.id.searchPlaceButton)
        val youAreHereButton: Button = findViewById(R.id.youAreHereButton)
        val infoBackButton: Button = findViewById(R.id.infoBackButton)
        val helpButton: Button = findViewById(R.id.infoHelpButton)

        button.setOnClickListener {
            val intent = Intent(this, SearchActivity::class.java)
            startActivity(intent)
        }

        youAreHereButton.setOnClickListener {
            val intent = Intent(this, YouAreHereActivity::class.java)
            startActivity(intent)
        }

        infoBackButton.setOnClickListener {
            finish()
        }

        helpButton.setOnClickListener {
            builder.show()
        }
    }
}