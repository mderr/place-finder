package com.example.placefinder

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity


class PlaceInfoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_place_info)

        val builder = AlertDialog.Builder(this)
        builder.setTitle("Información")
        builder.setMessage("Nuestro personal se acercará para ayudarte en breve")
        builder.setPositiveButton("Ok") { _, _ -> }

        val intent = intent
        val placeLabel: TextView = findViewById(R.id.place)
        val mapButton: Button = findViewById(R.id.mapButton)
        val directionsButton: Button = findViewById(R.id.directionsButton)
        val stepsButton: Button = findViewById(R.id.stepsButton)
        val placeName = intent.getStringExtra("place")
        val backButton: Button = findViewById(R.id.placeInfoBackButton)
        val helpButton: Button = findViewById(R.id.placeInfoHelpButton)

        placeLabel.text = placeName

        mapButton.setOnClickListener {
            val intent = Intent(this, MapsActivity::class.java)
            startActivity(intent)
        }

        directionsButton.setOnClickListener {
            val intent = Intent(this, RouteActivity::class.java)
            intent.putExtra("place", placeLabel.text.toString());
            startActivity(intent)
        }

        stepsButton.setOnClickListener {
            val intent = Intent(this, StepsActivity::class.java)
            intent.putExtra("place", placeLabel.text.toString());
            startActivity(intent)
        }

        backButton.setOnClickListener {
            finish()
        }

        helpButton.setOnClickListener {
            builder.show()
        }
    }
}