package com.example.placefinder

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AlertDialog

class SearchActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)

        val builder = AlertDialog.Builder(this)
        builder.setTitle("Información")
        builder.setMessage("Nuestro personal se acercará para ayudarte en breve")
        builder.setPositiveButton("Ok") { _, _ -> }

        val textPlace: EditText = findViewById(R.id.editTextPlace)
        val searchButton: Button = findViewById(R.id.searchButton)
        val typeButton: Button = findViewById(R.id.typeButton)
        val backButton: Button = findViewById(R.id.searchBackButton)
        val helpButton: Button = findViewById(R.id.searchHelpButton)

        searchButton.setOnClickListener {
            val intent = Intent(this, PlaceInfoActivity::class.java)
            intent.putExtra("place", textPlace.text.toString());
            startActivity(intent)
        }

        typeButton.setOnClickListener {
            showSoftKeyboard(textPlace)
        }

        backButton.setOnClickListener {
            finish()
        }

        helpButton.setOnClickListener {
            builder.show()
        }
    }

    private fun showSoftKeyboard(view: View) {
        if (view.requestFocus()) {
            val inputMethodManager: InputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
        }
    }
}