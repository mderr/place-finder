package com.example.placefinder

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AlertDialog

class StepsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_steps)

        val builder = AlertDialog.Builder(this)
        builder.setTitle("Información")
        builder.setMessage("Nuestro personal se acercará para ayudarte en breve")
        builder.setPositiveButton("Ok") { _, _ -> }

        val intent = intent
        val placeLabel: TextView = findViewById(R.id.stepsPlace)
        val placeName = intent.getStringExtra("place")
        val backButton: Button = findViewById(R.id.stepsBackButton)
        val helpButton: Button = findViewById(R.id.stepsHelpButton)

        placeLabel.text = placeName

        backButton.setOnClickListener {
            finish()
        }

        helpButton.setOnClickListener {
            builder.show()
        }
    }
}