package com.example.placefinder

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AlertDialog

class YouAreHereActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_you_are_here)

        val builder = AlertDialog.Builder(this)
        builder.setTitle("Información")
        builder.setMessage("Nuestro personal se acercará para ayudarte en breve")
        builder.setPositiveButton("Ok") { _, _ -> }

        val backButton: Button = findViewById(R.id.youAreHereBackButton)
        val helpButton: Button = findViewById(R.id.youAreHereHelpButton)

        backButton.setOnClickListener {
            finish()
        }

        helpButton.setOnClickListener {
            builder.show()
        }
    }
}